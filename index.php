<!DOCTYPE html>
<html>
<head>
	<title>PHP Editor</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="lib/codemirror/lib/codemirror.css">
    <link rel="stylesheet" type="text/css" href="lib/codemirror/theme/monokai.css">
  	<link rel="stylesheet" type="text/css" href="style.css">

    <script src="lib/jquery/jquery-3.6.0.min.js"></script>
    <script src="lib/codemirror/lib/codemirror.js"></script>
  	<script src="editor-action.js"></script>
    <script src="lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="lib/codemirror/mode/xml/xml.js"></script>
    <script src="lib/codemirror/mode/javascript/javascript.js"></script>
    <script src="lib/codemirror/mode/css/css.js"></script>
    <script src="lib/codemirror/mode/clike/clike.js"></script>
    <script src='lib/codemirror/mode/php/php.js'></script>
    <script src='lib/codemirror/addon/selection/active-line.js'></script>
    <script src='lib/codemirror/addon/edit/matchbrackets.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
</head>
<body>
    <div class="container">
        <!-- <h1>Just for personal purpose</h1> -->
        <div class="row my-3">
            <div class="col">
                <button type="button" id="run" class="btn btn-info rounded-0">Execute</button>
                <button type="button" id="clear" class="btn btn-warning rounded-0">Clear</button>
                <button type="button" id="refresh" class="btn btn-danger rounded-0">Refresh</button>
                <span id="error"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 mb-2">
                <textarea class="codemirror-textarea" id="code-editor"></textarea>
            </div>
            <div class="col-sm-6 col-xs-12">
                 <div id="result"></div>
            </div>
        </div>
    </div>
</body>
</html> 